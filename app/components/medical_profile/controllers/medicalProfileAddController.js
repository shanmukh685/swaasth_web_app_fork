(function() {
    'use strict';

    angular.module('swaasth.mprofile')
        .controller('medicalProfileAddController', ['$scope', '$rootScope', 'MedProfileService', 'ProfileService', '$stateParams', '$state', function($scope, $rootScope, MedProfileService, ProfileService, $stateParams, $state ) {
            console.log("medicalProfileAddController");

            $scope.selectedTab = "medical";
            $scope.switchedTab = 2;

            

            var param = $stateParams.params;
            var index = $stateParams.index, mData={};
            if(index){
                var details = MedProfileService.getStoredProfileDetails();
                // on refresh details will be empty, so get it from server
                if ( details.length == 0){
                    $state.go('medicalListView', {params: $state.params.params});
                }else{
                    mData = details.generalInfo[param][index];
                }
                
            }
            console.log("Parameters::-->", param,index,mData);

            var paramObject = swaasthRequestObj[param]; 
            $scope.headerText = paramObject.default.header;

            // $scope.maxTime = new Date().toISOString().substring(11,19);
            
            /*$scope.getAge = function(dob){
                var dob = new Date(dob);
                var year = dob.getFullYear(), month=dob.getMonth(), date = dob.getDate();
                console.log(year, month, date);
                var age = new Date().getFullYear() - year;
                var mdif = new Date().getMonth() - month;//0=jan   
                
                if(mdif < 0) {
                    --age;
                } else if(mdif == 0) {
                    var ddif = new Date().getDate() - date;
                    
                    if(ddif < 0) {
                        --age;
                    }
                }

                if(age == 0){
                    // age = moment().week()/52;
                    age = mdif/12;
                }
                console.log('dob -->', dob);
                console.log('age diff-->',age);
                return age;
            };*/

            

            $scope.getAge = function (dob) {
                var now = new Date();
                var born = new Date(dob);
                var birthday = new Date(now.getFullYear(), born.getMonth(), born.getDate());
                var age = '';
                if (now >= birthday) {
                    age = now.getFullYear() - born.getFullYear();
                } else {
                    age = now.getFullYear() - born.getFullYear() - 1;
                }

                console.log('age -->', age);

                return age;
            }

            var userId = localStorage.getItem('mProfileUserId');
            ProfileService.getUserDetails(userId).then(function(response) {
                if(response){
                    $scope.currentUser = response;                
                    $scope.currentUser.age = $scope.getAge($scope.currentUser.dateOfBirth);
                    $scope.minDate = new Date($scope.currentUser.dateOfBirth).toISOString().substring(0,10);
                    $scope.maxDate = new Date().toISOString().substring(0,10);
                    $scope.minDate = new Date($scope.currentUser.dateOfBirth).toISOString().substring(0,10);
                }
            });

            var formatedMesurementTime = '';
            if (mData.measurementTime) {
                formatedMesurementTime =  new Date(mData.measurementTime);
            } else {
                var year = new Date().getFullYear();
                var month = new Date().getMonth();
                var day = new Date().getDay();
                var hours = new Date().getHours();
                var minutes = new Date().getMinutes();
                formatedMesurementTime = new Date(year, month, day, hours, minutes, '00', '00'); //.format('hh:mm A');
            }

            $scope.medical = {
                value2: mData.value || '',
                unit2: paramObject.default.unit2,
                value1: '',
                unit1: paramObject.default.unit1,
                measurementDate: mData.measurementDate ? new Date(mData.measurementDate) : new Date(),
                measurementTime: formatedMesurementTime,
                note: mData.comments || ''
            }
            $scope.isShownNotes = false;

            if (param == 'bp' && mData.value) {
                /*This conditon is only for bp*/
                $scope.medical.value1 = mData.value.systolic;
                $scope.medical.value2 = mData.value.diastolic;
            }
            
            $scope.saveMedicalProfile= function(data){

                if ($scope.isShownNotes) {
                    $scope.isShownNotes = false;
                    return true;
                }

                $rootScope.showLoader();
                var requestParam = {};
                requestParam[param] = paramObject["request"];
                if (param == 'bp') {
                    /*This conditon is only for bp*/
                    requestParam[param].value.systolic = $scope.medical.value1;
                    requestParam[param].value.diastolic = $scope.medical.value2;
                } else {
                    requestParam[param].value = $scope.medical.value2;
                }
                requestParam[param].comments = $scope.medical.note;
                requestParam[param].measurementDate = $scope.medical.measurementDate;

                if (requestParam[param] && requestParam[param].measurementTime == '' || $scope.medical.measurementTime) {
                    requestParam[param].measurementTime = $scope.medical.measurementTime;
                }

                var reqObj = {};
                reqObj['authToken'] = localStorage.getItem('ngStorage-authToken');
                reqObj[param] = requestParam[param];
                if(index){
                    reqObj['type'] = "update";
                    reqObj['index'] = index;
                }
                reqObj.userId = localStorage.getItem('mProfileUserId');

                MedProfileService.save(reqObj).then(function(response) {
                    console.info('successfullly saved : : ', response);
                    $rootScope.hideLoader();
                    
                    if(index){
                        $state.go('medicalDetailView', { params:  param, index: index});
                       // $scope.goBack();
                    } else {
                        // $scope.goBack();
                        $state.go('medicalDetailView', { params:  param, index: response.result.length-1});
                    }

                }).catch(function(error) {
                    console.warn('error while saving : : ', error);
                });
            };

            $scope.showNotesPage = function () {
                $scope.isShownNotes = true;
            };

            

            $scope.focusInput1 = function () {
                if(param == 'height') {
                    $scope.heightPickFlag=true;
                    return true;
                }
                $('#medicalValue2')[0].focus();
            };

            $scope.focusTime = function () {
                $('#mTime')[0].focus();
            };
            $scope.focusDate = function () {
                $('#mDate')[0].focus();
            };

            $scope.setFeet = function ($event) {
                //$event.stopPropagation();
                //$event.preventDefault();

                if (param != 'height') {
                    return true;
                }

                $scope.feet = $event.target.innerHTML;
                var feet = $scope.feet? $scope.feet: "0'";
                var inch = $scope.inch? $scope.inch: '0"';

                console.log('feet:: ', feet, ' inch:: ', inch);
                $scope.medical.value1 = feet +' ' + inch ;
                console.log('$scope.medical.value1:: ', $scope.medical.value1);
                $scope.convertHeights();
            };

            $scope.setInch = function ($event) {

                if (param != 'height') {
                    return true;
                }

                //$event.stopPropagation();
                //$event.preventDefault();

                $scope.inch = $event.target.innerHTML;
                var feet = $scope.feet? $scope.feet: "0'";
                var inch = $scope.inch? $scope.inch: '0"';
                console.log('feet:: ', feet, ' inch:: ', inch);
                $scope.medical.value1 = feet +' ' + inch ;
                console.log('$scope.medical.value1:: ', $scope.medical.value1);
                $scope.convertHeights();
            };

            $scope.convertHeights = function(type, btemp){
                if(param == "height"){
                    $scope.medical.value2 = $scope.medical.value2>271.78?271.78:$scope.medical.value2;
                } else if(param == "weight"){
                    $scope.medical.value2 = $scope.medical.value2>650?650:$scope.medical.value2;
                } else if(param == "heartbeat"){
                    $scope.medical.value2 = $scope.medical.value2>240?240:$scope.medical.value2;
                } else if(param == "bodyTemperature"){
                    $scope.medical.value2 = $scope.medical.value2>41?41:$scope.medical.value2;
                    $scope.medical.value1 = $scope.medical.value1>106?105.8:$scope.medical.value1;
                } else if(param == "bp"){
                    $scope.medical.value1 = $scope.medical.value1>220?220:$scope.medical.value1;
                    $scope.medical.value2 = $scope.medical.value2>130?130:$scope.medical.value2;
                }

                if (param == 'height') {
                    var retValue = 0;
                    if(type=="CM"){
                        /*
                         * converting cm to inch
                        */
                        var initVal = ($scope.medical.value2/30.48);
                        var feet = Math.floor(initVal);
                        var inch = Math.round((initVal%1)*12);
                        retValue = feet +"'" +" "+inch+'"';
                        $scope.medical.value1 = retValue;
                        console.log('inch conversion::', $scope.medical.value1);
                    } else {

                        /*
                         * converting inch to cm
                        */
                        var feet = $scope.medical.value1.split(" ")[0];
                        var inch = $scope.medical.value1.split(" ")[1];

                        var feetInch = parseInt(feet) + (parseInt(inch))/12;
                        retValue = parseFloat((feetInch)*30.48);
                        $scope.medical.value2 = parseFloat(retValue.toFixed(2));
                        console.log('cm conversion::', $scope.medical.value2);
                    }

                } else if (param == 'bodyTemperature'){
                    var returnVal = 0;
                    if(btemp == "FN"){
                        /*
                         * converting F to C
                        */
                        var initVal = ($scope.medical.value1 - 32) * 0.555555556;
                        $scope.medical.value2 = Number(initVal.toFixed(2));
                        console.log('inch conversion::', $scope.medical.value1);
                        
                    } else {
                        /*
                         * converting C to F
                        */
                        var initVal = ($scope.medical.value2 * 1.8) + 32;
                        $scope.medical.value1 = Number(initVal.toFixed(2));
                        console.log('inch conversion::', $scope.medical.value1);
                    }

                } else {
                    return true;
                }


            };

            if (param == 'height' && mData.value) {
                $scope.convertHeights("CM");
            } else if (param == 'bodyTemperature' && mData.value) {
                $scope.convertHeights("CM", "CL");
            }

    }]);

})();

            