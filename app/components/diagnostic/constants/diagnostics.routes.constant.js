/*globals angular */
(function() {
    'use strict';

    /**
     * @description 
     * Contains the Routes Constants, which are available across the swaasth.profile module.
     * 
     * @author 
     * Tushar
     */
    angular.module('swaasth.diagnostic').constant('diagnosticRoutes', {

        /**********************************************************************
                    PROFILE ROUTES
        **********************************************************************/

        diagnosticView: {
            name: 'diagnosticView',
            url: '/diagnosticView'
        },
        
        testTypeView: {
            name: 'testTypeView',
            url: '/testTypeView/:id?name'
        },
 
        diagnosticsList: {
            name: 'diagnosticsList',
            url: '/diagnosticsList/:params/:testName/:testId/:unitId'
        },
 
        diagnosticDetailView: {
            name: 'diagnosticDetailView',
            url: '/diagnosticDetailView/:params/:testName/:testId/:unitId?index'
        },
 
        addDiagnosticView: {
            name: 'addDiagnosticView',
            url: '/addDiagnosticView/:params/:testName/:testId/:unitId?index'
        }

    });
})();
