/**
 * @description 
 * get the home recommendation
 *              
 * @author Koushik
 */
(function() {

    'use strict';

    var HomeService = function($q, $http, $localStorage, BASE_URL, API) {

        var notification = [];


        notification.getNotifications = function(auth) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.getNotifications, auth)
                .then(function(res) {
                    console.info('Notifications : : ', res);

                    if (res.data.statusCode === 200) {
                        notification = res.data.notifications;
                        //localStorage.setItem('notification', JSON.stringify(notification));
                        deferred.resolve(res.data);
                    } else {
                        deferred.reject(res.data);
                    }

                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        notification.getDetailNotifications = function(id) {

            var notify = [];
            if (notification.length) {
                notify = notification;
            } else {
                notify = JSON.parse(localStorage.getItem('notification'));
            }

            var detailNotification = _.where(notify, {'id': id});
            return detailNotification;

        }


        return notification;
    };

    HomeService.$inject = [
        '$q',
        '$http',
        '$localStorage',
        'BASE_URL',
        'API'
    ];


    angular
        .module('swaasth.home')
        .factory('HomeService', HomeService);
})();
