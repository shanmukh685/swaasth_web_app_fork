/**
 * @description:: Dashboard controller
 * @author::Bhaskar
 */

'use strict';

angular.module('swaasth.mprofile')
.controller('dashboardController', ['$scope', '$rootScope', '$state', '$stateParams', 'MedProfileService', 'ProfileService', function($scope, $rootScope, $state, $stateParams, MedProfileService, ProfileService) {
    console.log("dashboardController");


    $scope.selectedTab = "dashboard";
    $scope.switchedTab = 2;

    var param = $stateParams.params;
    console.log('dashboardController params', param);

    $scope.getFamilyMember = function () {
    	$rootScope.showLoader();
        ProfileService.getUserDetails(param).then(function(response) {
            if(response){
                localStorage.setItem('mProfileUserId', response.id);
                $scope.currentUser = response;
            }
            $scope.getBMI();
        });
    };
    $scope.getFamilyMember();
    

    $scope.showInfo = function($event){
        $event.stopPropagation();
        $event.preventDefault();
    	$scope.isShowInfo1 = true;
    }
	$scope.hideInfo = function($event){
        $event.stopPropagation();
        $event.preventDefault();
    	$scope.isShowInfo1 = false;
    }

    $scope.showPersonalDetail = function () {
        $state.go('familyMemberDetail', { params: param });
    };

    $scope.goToMedicalInfo = function(){
        $state.go('medicalinfo', { params: param });
    };

    $scope.getBMI = function() {
        $rootScope.showLoader();

        var reqObj = {};
        reqObj.authToken = localStorage.getItem('ngStorage-authToken');
        reqObj.userId = localStorage.getItem('mProfileUserId');

        console.log(reqObj);
        MedProfileService.getBMI(reqObj).then(function(response) {
            console.info('got bmi : : ', response);
            if (response.statusCode === 200){
                $scope.bmiData = response.result;

                var initVal = ($scope.bmiData.height.value/30.48);
                var feet = Math.floor(initVal);
                var inch = Math.round((initVal%1)*12);
                $scope.bmiData.height.value = feet +"'" +" "+inch+'"';
            }
            $rootScope.hideLoader();
        }).catch(function(error) {
            console.warn('error while getting bmi : : ', error);
            $rootScope.hideLoader();
        });

    };
    

}]);