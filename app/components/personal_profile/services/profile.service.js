/**
 * @description 
 * Saves the user details
 *              
 * @author Tushar
 */
(function() {

    'use strict';

    var ProfileService = function($q, $http, $localStorage, BASE_URL, API) {

        var profile = {};
        profile.familyMemberList = [];

        profile.browseImage = function(source) {
            var base64Content = '';
            var allowedExtensions = ["jpg", "png", "jpeg", "image/jpg", "image/png", "image/jpeg"];
            var cameraOptions = {
                quality: 100,
                destinationType: Camera.DestinationType.DATA_URL,
                allowEdit: true,
                targetWidth: 100,
                targetHeight: 100,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                sourceType: source === "gallary" ? Camera.PictureSourceType.PHOTOLIBRARY : Camera.PictureSourceType.CAMERA,
                mediaType: navigator.camera.MediaType.PICTURE,
                encodingType: Camera.EncodingType.PNG
            }
            var deferred = $q.defer();

            if (navigator.camera) {

                navigator.camera.getPicture(function(imageURI) {
                    imageURI = 'data:image/png;base64,' + imageURI;
                    deferred.resolve(imageURI);
                }, function(error) {
                    alert("Error while fetching Image from Gallary :: " + error);
                    deferred.reject(error);
                }, cameraOptions);

                return deferred.promise;
            } else {
                // return alert('Only supported in tablets.');
                navigator.camera.getPicture(function(imageURI) {
                    window.resolveLocalFileSystemURL(imageURI, function(imageData) {
                        imageData.file(function(imageInfo) {
                            var reader = new FileReader();

                            reader.onloadend = function(evt) {
                                base64Content = reader.result;
                                deferred.resolve(base64Content);
                            }
                            reader.readAsDataURL(imageInfo);

                        });

                    }, function(error) {
                        console.warn('Error while locating file : : ', error);
                        deferred.reject(error);
                    });

                }, function(error) {
                    deferred.reject(error);
                }, cameraOptions);
            }

            return deferred.promise;

        };


        profile.save = function(user, screenNumber) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.updatePersonalInfo, user)
                .then(function(res) {
                    console.info('ProfileService [save] : : ', res);
                    if (res.data.statusCode === 200) {
                        // $localStorage.currentUser = res.data.userDetails;
                        // if ($localStorage.screenNumber) {
                        //     $localStorage.screenNumber = screenNumber;
                        // } else {
                        //     $localStorage.$default({
                        //         screenNumber: screenNumber
                        //     });
                        // }
                        deferred.resolve(res.data);
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

       /* profile.getlastUnsavedScreenNumber = function() {
            return $localStorage.screenNumber;
        };*/

       /* profile.setProfileProgress = function(progress) {
            $localStorage.profileProgress = +progress;
        };

        profile.getProfileProgress = function() {
            return $localStorage.profileProgress;
        };*/

        profile.getFamilyMemberList = function() {
            var deferred = $q.defer();

            var reqObj = {
                authToken: localStorage.getItem('ngStorage-authToken')
            };

            $http
                .post(BASE_URL.url + API.getFamilyMember, reqObj)
                .then(function(res) {
                    console.info('Family memeber list [get] : : ', res);
                    if (res.data.statusCode === 200) {
                        profile.familyMemberList = res.data.result;
                        deferred.resolve(res.data);
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;

        };

        profile.getUserDetails = function(id) {
            var deferred = $q.defer();

            var reqObj = {
                authToken: localStorage.getItem('ngStorage-authToken'),
                userId: id
            };

            $http
                .post(BASE_URL.url + API.getUserDetail, reqObj)
                .then(function(res) {
                    console.info('User Details [get] : : ', res);
                    if (res.data.statusCode === 200) {
                        deferred.resolve(res.data.data);
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        profile.deleteMember = function(reqObj) {
            var deferred = $q.defer();

            $http.post(BASE_URL.url + API.deleteMember, reqObj)
                .then(function(res) {
                    console.info('Family deleteMember  response : ', res);
                    if (res.data.statusCode === 200) {
                        profile.familyMemberList = res.data.result;
                        deferred.resolve(res.data);
                    } else {
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;

        };


        return profile;
    };

    ProfileService.$inject = [
        '$q',
        '$http',
        '$localStorage',
        'BASE_URL',
        'API'
    ];


    angular
        .module('swaasth.profile')
        .factory('ProfileService', ProfileService);
})();
