(function() {

    'use strict';


    var handleDeviceBackButton = function() {
        angular.element(document.body).scope().goBack();
        console.log('Back Button working');
    };

    var onDeviceReady = function() {
        console.log("Device Ready Fired!!");
        angular.bootstrap(document,['swaasth']);
        //angular.element(document.body).scope().setDeviceReady();
        document.addEventListener("backbutton", handleDeviceBackButton, false);
    };

    document.addEventListener('deviceready', onDeviceReady);

    document.addEventListener("backbutton", onBackKeyDown, false);

    function onBackKeyDown(event) {
        event.preventDefault();
        if($("#addUnitModal").hasClass('in')) {
            $(".in").hide();
        } else if ($("#addNewUnitModal").hasClass('in')) {
            $(".in").hide();
        } else if ($("#deleteModal").hasClass('in')) {
            $(".in").hide();
        } else if ($("#addNewAllery").hasClass('in')) {
            $(".in").hide();
        }
    }


    // Declare app level module which depends on views, and js
    var swaasth = angular.module('swaasth', [
        'ngCordova',
        'ui.router',
        'ngStorage',
        'ngMessages',
        'ngAnimate',
        'dc.inputAddOn',
        'kendo.directives',
        'angular-carousel',
        'ui.bootstrap',
        'jkuri.datepicker',
        'swaasth.home',
        'swaasth.record',
        'swaasth.auth',
        'swaasth.profile',
        'swaasth.mprofile',
        'swaasth.dashboard',
        'swaasth.allergies',
        'swaasth.diagnostic',
        'swaasth.snapshots',
        'swaasth.record',
        'swaasth.healthinsurance'        
    ]).config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.interceptors.push('APIInterceptor');

        // $urlRouterProvider.otherwise("/home");

    }]).run(['$rootScope', '$state', '$location', '$localStorage', function($rootScope, $state, $location, $localStorage) {

        $rootScope.$state = $state;

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, error) {

            $rootScope.animation = toState.animation;
            $rootScope.currentState = toState.name;
            $rootScope.previousState = fromState.name;

/*            var isShownLogo = [
                'resetPassword',
                'forgotPassword',
                'signUp',
                'setPassword',
                'resetPassword',
                'otp',
                'changePassword'
            ];*/

            if (toState.name != 'login') {
                $rootScope.showLogo = false;
            } else {
                $rootScope.showLogo = true;
            }

        });

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            console.log('bar');
            if (toState.name == 'home') {
                $state.go('home.recommendationsList');
            }
        });

        $rootScope.showLoader = function(){
            $rootScope.loader = true;
        };

        $rootScope.hideLoader = function(){
            $rootScope.loader = false;
        };
        
        $rootScope.signUpObj = {
                emailId : '',
                contactNumber:'',
                fromSignUp:false
        };
        /* $rootScope.$on('$stateChangeSuccess', function(event, toState, fromState) {

         });*/

        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
            
            event.preventDefault();
            console.info('stateChangeError :: ', error);
            /*if (error === "authenticated") {
                if ($localStorage.isFirstLogin === 'yes') {
                    // $state.go('resetPassword');
                    // $state.go('login');
                } else {
                    var lastUnsavedScreenNumber = $localStorage.screenNumber;
                }
            }*/
        });

    }]);

})();
