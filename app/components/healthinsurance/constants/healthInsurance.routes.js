/*globals angular */
(function() {
    'use strict';

    /**
     * @description 
     * Contains the Routes Constants, which are available across the swaasth.profile module.
     * 
     * @author 
     * Tushar
     */
    angular.module('swaasth.healthinsurance').constant('HealthInsuranceRoutes', {

        /**********************************************************************
                    Insurance ROUTES
        **********************************************************************/

        healthInsurance: {
            name: 'healthInsurance',
            url: '/healthInsurance/:params'
        },

        healthInsuranceListView: {
            name: 'healthInsuranceListView',
            url: 'healthInsuranceListView/:params'
        },

        healthInsuranceDetailView: {
            name: 'healthInsuranceDetailView',
            url: 'healthInsuranceDetailView/:params'
        }

    });
})();