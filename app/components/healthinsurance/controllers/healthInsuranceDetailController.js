(function() {
    'use strict';

    angular.module('swaasth.healthinsurance')
        .controller('healthInsuranceDetailController', ['$scope', '$rootScope', 'InsuranceService', '$stateParams', '$state', 'ProfileService', function($scope, $rootScope, InsuranceService, $stateParams, $state, ProfileService) {
            
            $scope.selectedTab = "record";
            $scope.switchedTab = 1;

            if($state.current.name == 'healthInsuranceDetailView'){
                $('#indexCloud').addClass('cloud-light');
            }else{
                $('#indexCloud').removeClass('cloud-light');
            }
            console.log("healthInsuranceDetailController Called", $stateParams.params); 
            $scope.headerText = 'Insurance';
            $scope.insuranceDetails = JSON.parse($stateParams.params);

            $scope.editInsuranceInfo = function() {
            	$state.go('healthInsurance', { params:  JSON.stringify($scope.insuranceDetails)});
            }

            $scope.deleteInsuranceInfo = function() {
                $rootScope.showLoader();
                $scope.insuranceDetails.authToken =  localStorage.getItem('ngStorage-authToken');
                
                InsuranceService.deleteInsurance($scope.insuranceDetails).then(function(){
                    $rootScope.hideLoader();
                    $state.go('healthInsuranceListView', { params:  JSON.stringify($scope.insuranceDetails)});
                }).catch(function(error) {
                    $rootScope.hideLoader();
                    console.log('error while deleting the insurance', error);
                });                
            }

            $scope.goBack = function() {
                $state.go('healthInsuranceListView', { params:  JSON.stringify($scope.insuranceDetails)});
            }
    }]);

})();