(function() {
    'use strict';

	angular.module('swaasth.healthinsurance')
        .controller('healthInsuranceController', ['$scope', '$rootScope', 'InsuranceService', 'ProfileService', '$stateParams', '$state', 
            function($scope, $rootScope, InsuranceService, ProfileService, $stateParams, $state ) {
                console.log("healthInsuranceController");      
                $scope.insuranceDetails = $stateParams.params != ""? JSON.parse($stateParams.params) : {};
                
                $scope.selectedTab = "record";
                $scope.switchedTab = 1;

                var param = $stateParams.params;
                console.log('healthInsuranceController param::', param);
                console.log('healthInsuranceController state::', $state);
                $rootScope.showLoader();
                if($state.current.name == 'healthInsurance'){
                    $('#indexCloud').addClass('cloud-light');
                }else{
                    $('#indexCloud').removeClass('cloud-light');
                }
                $scope.getFamilyMember = function () {
                    ProfileService.getFamilyMemberList().then(function(res){
                        
                        $rootScope.hideLoader();                
                        $scope.members = res.result;
                        $scope.members.push({user : {firstName:'Others'}});
                        console.log('get family member info-->', $scope.members);                        
                    }).catch(function(error){
                        $rootScope.hideLoader();
                        console.log('error while getting family members list', error);
                    });

                };
                $scope.getFamilyMember();

                $scope.headerText = 'Insurance';//param == 'edit' ? 'Done' : 'Next';
                $scope.insuranceProviders =  InsuranceService.getStoredInsuranceProviders();                
                $scope.policyTypes = InsuranceService.getStoredPolicyTypes();
                $scope.policyTerms = InsuranceService.getStoredPolicyTerms();
                


                $scope.saveInsuranceDetail = function(){
                	// Save Insurance Details
                    $rootScope.showLoader();                    
                    $scope.insuranceDetails.authToken =  localStorage.getItem('ngStorage-authToken');                   
                	console.log("Save Insurance Button clicked", $scope.insuranceDetails);
                    InsuranceService.saveInsurance($scope.insuranceDetails).then(function(res){
                        if (res.statusCode==200){
                            $rootScope.hideLoader();
                            $state.go('healthInsuranceDetailView', {params:  JSON.stringify(res.Insurance)});
                        }
                    }).catch(function(error) {
                        console.log('error while saving Insurance', error);
                    });
                }   

                $scope.applyClass = function(val) {
                        if (val != "") {
                            return "form-input calendar-cls";
                        } else {
                            return "form-input"; // Or even "", which won't add any additional classes to the element
                        }
                }

                $scope.goBack = function() {
                    $state.go('healthInsuranceListView', { params:  JSON.stringify($scope.insuranceDetails)});
                }

	       }]);
})();